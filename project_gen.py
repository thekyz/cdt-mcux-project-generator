#!/usr/bin/env python3

# MIT License
# 
# Copyright (c) 2018 Alex Garcia
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import jinja2
import argparse
import os
import sys
import json

def _extract_include_folders(json_data):
    if not 'inc' in json_data:
        raise RuntimeError('Could not find "inc" parameter in flags json')

    return [ '../' + i for i in json_data['inc'].split() ]

def _get_all_subfolders(f):
    ''' Returns all subfolders and root from a full path (i.e. would return ( 'a', [ 'a', 'a/b', 'a/b/c' ] ) for input 'a/b/c/d.o' '''
    subs = []
    root = ''
    sub = os.path.dirname(f)

    if sub != f and sub:
        root, subs = _get_all_subfolders(sub)

    if sub:
        subs.append(sub)
    else:
        root = f

    return root, subs

def _extract_source_data(json_data, build_prefix, includes):
    if not 'objs' in json_data:
        raise RuntimeError('Could not find "objs" parameter in flags json')

    files = []
    folders = []
    root_folders = []
    for f in json_data['objs'].split():
        c_file = f.replace(build_prefix, '').replace('.o', '.c')
        root, subs = _get_all_subfolders(c_file)

        parent = 0
        for i in includes:
            base_i = i.replace('../', '')
            root_i, _ = _get_all_subfolders(base_i)
            # print('  ' + base_i, root_i)
            if root == root_i:
                parent = i.count('../') - 1
                break

        if parent:
            # print(c_file, root, parent)
            folders.extend(subs)
            root_folders.append(root)
            files.append({ 'path': c_file, 'parent': 'PARENT-' + str(parent) + '-PROJECT_LOC' })

    folders = sorted(set(folders))
    root_folders = sorted(set(root_folders))

    source_data = {
        'files': files,
        'folders': folders,
        'root_folders': root_folders,
    }

    return source_data

def _extract_flag(cmd_flag, flags_type):
    if flags_type == 'compiler':
        opt_prefix = 'gnu'
    else:
        opt_prefix = 'link'

    flag = {}
    if cmd_flag == '-nostdinc':
        flag = { 'nostdinc': 'true' }
    elif cmd_flag == '-Os':
        flag = { 'optimization_level': opt_prefix + '.c.optimization.level.size' }
    elif cmd_flag == '-Og':
        flag = { 'optimization_level': opt_prefix + '.c.optimization.level.general' }
    elif cmd_flag == '-Wall':
        flag = { 'wall': 'true' }
    elif cmd_flag == '-flto':
        flag = { 'lto': 'true' }
    elif cmd_flag == '-ffat-lto-objects':
        flag = { 'lto_fat': 'true' }
    elif cmd_flag == '-mthumb':
        flag = { 'thumb': 'true' }
    elif cmd_flag == '-nodefaultlibs':
        flag = { 'nodeflibs': 'true' }
    elif cmd_flag == '-nostdlib':
        flag = { 'nostdlib': 'true' }
    elif cmd_flag == '-static':
        flag = { 'static': 'true' }
    elif cmd_flag == '-fno-common':
        flag = { 'nocommon': '-fno-common' }
    elif cmd_flag == '-g' or cmd_flag == '-g2':
        flag = { 'debug_level': opt_prefix + '.c.debugging.level.default' }
    elif cmd_flag == '-g1':
        flag = { 'debug_level': opt_prefix + '.c.debugging.level.minimal' }
    elif cmd_flag == '-g3':
        flag = { 'debug_level': opt_prefix + '.c.debugging.level.max' }
    elif "redlib.specs" in cmd_flag:
        # we are kinda ignoring that value as it's being passed by default, without any option, by mcu build
        flag = { 'redlib_spec': 'true' }

    return flag

def _extract_compiler_data(json_data):
    if not 'cflags' in json_data:
        raise RuntimeError('Could not find "cflags" parameter in flags json')

    defines = []
    flags = {}
    misc_flags = ''
    for e in json_data['cflags'].split():
        if e.startswith('-D'):
            defines.append(e[2:])
        else:
            flag = _extract_flag(e, 'compiler')

            if flag:
                flags.update(flag)
            else:
                misc_flags += e + ' '

    print('    --- misc compiler flags (not used as mcu options): ' + misc_flags)

    compiler_data = {
        'defines': defines,
        'flags': flags,
        'misc_flags': misc_flags,
    }
    return compiler_data

def _extract_linker_data(json_data):
    if not 'ldflags' in json_data:
        raise RuntimeError('Could not find "ldflags" parameter in flags json')

    options = []
    flags = {}
    xlinker = False
    misc_flags = ''
    for e in json_data['ldflags'].split():
        if xlinker:
            xlinker = False
            if e.startswith('-Map'):
                e = '-Map=&quot;' + os.path.basename(e[5:]) + '&quot;'
            options.append(e)
        elif e.startswith('-T'):
            linker_file = e[2:]
        elif e.startswith('-L'):
            linker_path = e[2:].replace('apps/', '')
        elif e.startswith('-Xlinker'):
            xlinker = True
        else:
            flag = _extract_flag(e, 'linker')

            if flag:
                flags.update(flag)
            else:
                misc_flags += e + ' '

    print('    --- misc linker flags (not used as mcu options): ' + misc_flags)

    linker_data = {
        'filename': linker_file,
        'path': linker_path,
        'flags': flags,
        'options': options,
        'misc_flags': misc_flags,
    }
    return linker_data

def _extract_data(json_data, build_prefix, target_name):
    print('  > extracting data from "' + target_name + '" build ...')
    includes = _extract_include_folders(json_data)
    data = {
        'includes': includes,
        'source_data': _extract_source_data(json_data, build_prefix, includes),
        'compiler_data': _extract_compiler_data(json_data),
        'linker_data': _extract_linker_data(json_data),
    }
    return data

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate project files for MCUXpresso based on Makefile flags')
    parser.add_argument('--template-cproject', help='Base template for the .cproject file', required=True)
    parser.add_argument('--template-project', help='Base template for the .project file', required=True)
    parser.add_argument('--release-flags', help='Path to a file containing flags (cflags, include folders, and object files) for the release target', required=True)
    parser.add_argument('--debug-flags', help='Path to a file containing flags (cflags, include folders, and object files) for the debug target', required=True)
    parser.add_argument('--app-name', help='Application name', required=True)
    parser.add_argument('--bsp-name', help='BSP name (default: none)', default='none')
    parser.add_argument('--output-dir', help='Directory where the project files will be generated', required=True)
    args = parser.parse_args()

    if not os.path.isfile(args.template_project):
        print('--- error: the .project template (' + args.template_project + ') is not readable')
        sys.exit(1)

    if not os.path.isfile(args.template_cproject):
        print('--- error: the .cproject template (' + args.temlate_cproject + ') is not readable')
        sys.exit(1)

    if not os.path.isfile(args.debug_flags):
        print('--- error: the debug flags file (' + args.debug_flags + ') is not readable')
        sys.exit(1)

    if not os.path.isfile(args.release_flags):
        print('--- error: the release flags (' + args.release_flags + ') is not readable')
        sys.exit(1)

    if not os.path.isdir(args.output_dir):
        print('--- error: the output folder (' + args.output_dir + ') is not readable')
        sys.exit(1)
    args.output_dir = os.path.join(args.output_dir, '')

    print('+++ parsing input data ...')

    with open(args.debug_flags) as debug_flags_file:
        debug_flags_json = json.load(debug_flags_file)
    with open(args.release_flags) as release_flags_file:
        release_flags_json = json.load(release_flags_file)

    build_prefix_base = '../../build/' + args.app_name + '/' + args.bsp_name + '_'
    debug_data = _extract_data(debug_flags_json, build_prefix_base + 'debug/', 'debug')
    release_data = _extract_data(release_flags_json, build_prefix_base + 'release/', 'release')

    print('+++ generating project files ...')

    bsp_name = args.bsp_name
    app_name = args.app_name

    project = {
        'app_name': app_name,
        'bsp_name': bsp_name,
        'configurations': [
            { 'name': app_name + '_' + bsp_name + '_debug', 'type': 'debug', 'build_data': debug_data },
            { 'name': app_name + '_' + bsp_name + '_release', 'type': 'release', 'build_data': release_data },
        ]
    }

    with open(args.template_cproject) as template_file:
        cproject_template = jinja2.Template(template_file.read())

    with open(args.output_dir + '.cproject', 'w') as cproject_file:
        cproject_file.write(cproject_template.render(project))

    with open(args.template_project) as template_file:
        project_template = jinja2.Template(template_file.read())

    with open(args.output_dir + '.project', 'w') as project_file:
        project_file.write(project_template.render(project))
